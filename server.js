var express = require('express'); // referencia al paquete express
var app = express();

//Operacion GET del "hola mundo"
app.get ('/',function(request,response){
  response.send('Hola mundo');
});

app.listen(3000,function() {
  console.log('Node JS escuchando en el puerto 3000');
});
